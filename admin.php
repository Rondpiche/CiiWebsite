<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CII</title><link rel="icon"
 type="image/png"
 href="asset\Cii logo.png">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Theme CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <!-- angular -->
    <script src="js/angular.js"></script>
    <!--script and css to create account and log in-->
    <script type="text/javascript" src="admin-management/adminManagement.js"></script>
    <script type="text/javascript" src="admin-management/sha512.js"></script>
    <link href="css/form.css" rel="stylesheet">
</head>
<body ng-app="main_ng-app">

  <?php include("asset/topbar.php") ?>

  <div class="container" style="margin-top:150px" >
    	<div class="row">
			<div class="col-md-6 col-md-offset-3" >
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">

                <!--login form-->
                <form id="login-form" name="myForm_logIn" ng-controller="form_controller">
                  <div class="form-group">
                		<input id=usernameLogIn name="name_input_logIn" ng-model="name" type="text" ng-maxlength=20 tabindex="1" class="form-control" placeholder="username" required>
                		  <div ng-show="!myForm_logIn.name_input_logIn.$valid && submit ==1" class='callout alert' >A username must be given</div>
                      <div ng-show="myForm_logIn.name_input_logIn.$error.maxlength && myForm_logIn.name_input_logIn.$touched" class='callout alert'>username to long</div>
                  </div>
              		<div class="form-group">
                		<input id="passwordLogIn" name="password_input_logIn" ng-model="password" type="password" ng-minlength=3 ng-maxlength=255 class="form-control" placeholder="password" required>
                      <div ng-show="!myForm_logIn.password_input_logIn.$valid && submit ==1 && !myForm_logIn.password_input_logIn.$error.minlength && !myForm_logIn.password_input_logIn.$error.maxlength" class='callout alert' >A password must be given</div>
                      <div ng-show="myForm_logIn.password_input_logIn.$error.maxlength && myForm_logIn.password_input_logIn.$touched" class='callout alert'>password to long</div>
                      <div ng-show="myForm_logIn.password_input_logIn.$error.minlength && myForm_logIn.password_input_logIn.$touched" class='callout alert'>password must be at least 3 characters</div>
                  </div>
                  <div ng-show="<?php if(isset($_GET["logInFail"]) && !empty($_GET["logInFail"])) {echo $_GET["logInFail"];}else{echo 0;} ?>" class='callout alert'>your username or your password is invalid</div>
                  <input ng-click="submitDataLogIn()" name="log_input" type="submit" class="form-control btn btn-login" value="Log In" />
                </form>
                <!--Angular js controller-->
                <script>
                var app = angular.module('main_ng-app', []);
                	app.controller('form_controller', function($scope) {
                    //log in
                    $scope.submitDataLogIn = function(){
                      if($scope.myForm_logIn.$valid){
                        logIn();
                      }
                    }
                  });
                </script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
