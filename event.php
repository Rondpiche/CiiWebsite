<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CII</title><link rel="icon"
 type="image/png"
 href="asset\Cii logo.png">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Theme CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <!-- angular -->
    <script src="js/angular.js"></script>
    <!--script and css to create account and log in-->
    <script type="text/javascript" src="admin-management/adminManagement.js"></script>
    <script type="text/javascript" src="admin-management/sha512.js"></script>
    <link href="css/form.css" rel="stylesheet">
</head>
<body >

  <?php include("asset/topbar-home.php") ?>
  <!-- Page Header -->
  <header class="intro-header" style="background-image: url('asset/home-page.jpg'); margin-bottom: 0px;" >
      <div class="container">
          <div class="row">
              <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                  <div class="site-heading">
                      <h1 style="font-size: 70px;">CII</h1>
                      <hr style="max-width: 300px;" class="small">
                      <span class="subheading">Club Informatique de l'Isen</span>
                  </div>
              </div>
          </div>
      </div>
  </header>

  <?php include("event-management/displayEvent.php") ?>

</body>
<style>
  p{
      font-size: 25px;
  }
  .h1, .h2, .h3, h1, h2, h3 {
      font-size: 45px;
  }
  .date-display{
    font-size: 18px;
  }
</style>
</html>
