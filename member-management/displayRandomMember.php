<?php
  $fileSettings = fopen("settings.txt", "r");
  $firstLine = intval(fgets($fileSettings));
  $databaseUsername = fgets($fileSettings);
  $databasePassword = fgets($fileSettings);
  $databaseName = fgets($fileSettings);
  $databaseUsername = substr($databaseUsername, 0, -2);
  $databasePassword = substr($databasePassword, 0, -2);
  fclose($fileSettings);
  try{
      $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
  }
  catch (Exception $e){
      die('Erreur : ' . $e->getMessage());
  }
  $temp2 = $bdd->query("SELECT * FROM member");
  $numbermember =0;
  while ($temp1 = $temp2->fetch()){
    $numbermember++;
  }
  $randomId =rand(1, $numbermember);
  $reponse = $bdd->query("SELECT * FROM member");
  $i =0;
  while ($raw_data = $reponse->fetch()){
    $i++;
    if($i ==$randomId){
      echo '<div class="col-lg-1"></div>
      <div class="col-lg-4" style="text-align: center;margin-top: 80px;margin-bottom: 80px;">
       <img src="'.$raw_data["picture_path"].'" alt="member" class="img-circle" width="250px" height="250px">
       <div class="col-lg-2"></div>
      </div>
      <div class="col-lg-5" style="text-align: left;margin-top: 80px;margin-bottom: 40px;">
        <h3>Nos membres:</h3>
        <h1>'.$raw_data["name"].'</h1>
        <p>'.$raw_data["role"].'</p>
        <p><a class="btn btn-default" href="membre.php" role="button">Regarder le profil de nos autres membre &raquo;</a></p>
       </div>';
    }
  }
  if($i==0){
    echo '<div class="col-lg-1"></div>
    <div class="col-lg-4" style="text-align: center;margin-top: 80px;margin-bottom: 80px;">
     <img alt="pas de membre enregistré" class="img-circle" width="250px" height="250px">
     <div class="col-lg-2"></div>
    </div>
    <div class="col-lg-5" style="text-align: left;margin-top: 80px;margin-bottom: 40px;">
      <h3>Nos membres:</h3>
      <h1>Pas de membre enregistré</h1>
      <p> </p>
      <p><a class="btn btn-default" href="membre.php" role="button">Regarder le profil de nos autres membre &raquo;</a></p>
     </div>';
  }

?>
