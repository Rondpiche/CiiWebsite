//Log in
function logIn(){
      //form submit
      var form = document.createElement("form");
      form.method = 'post';
      form.action = 'admin-management/adminManagement_logIn.php';

      var username = document.createElement('input');
      username.type = "text";
      username.name = "username";
      username.value = document.getElementById('usernameLogIn').value;
      form.appendChild(username);

      var password = document.createElement('input');
      password.type = "text";
      password.name = "password";
      password.value = SHA512(document.getElementById('passwordLogIn').value);
      form.appendChild(password);

      form.setAttribute("style", "display : none");
      document.body.appendChild(form);
      form.submit();
}
//logOut
function logOut(){
    window.location.href = "admin-management/adminManagement_logOut.php";
}
