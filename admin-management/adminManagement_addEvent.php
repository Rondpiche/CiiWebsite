<?php
//check if at least one file was send
session_start();
if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
  if (isset($_FILES["fileUploadHidden"]["error"]) !=false){
    //go though every files
    foreach ($_FILES["fileUploadHidden"]["error"] as $key => $error) {
      if ($error != UPLOAD_ERR_OK) {
        //not upload correctly
        break;
      }
      $extension =substr($_FILES['fileUploadHidden']['name'][$key] ,stripos($_FILES['fileUploadHidden']['name'][$key], '.') , strlen($_FILES['fileUploadHidden']['name'][$key]));
      if( !(($extension ==".jpg")||($extension ==".png")) ){
        //not the correct exention
        break;
      }
      if($_FILES['fileUploadHidden']['size'][$key] >10000000){
        //to Big
        break;
      }

      //!ADD MORE TEST LATER!//
      //pepare to send
      $fileSettings = fopen("../settings.txt", "r");
      $firstLine = intval(fgets($fileSettings));
      $databaseUsername = fgets($fileSettings);
      $databasePassword = fgets($fileSettings);
      $databaseName = fgets($fileSettings);
      $databaseUsername = substr($databaseUsername, 0, -2);
      $databasePassword = substr($databasePassword, 0, -2);
      fclose($fileSettings);
      try{
        $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
      }
      catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
      }
      //retrive var
      $reponse = $bdd->query("SELECT * FROM event");
      $id =1;//default value
      while($raw_data = $reponse->fetch()){
        $id =$raw_data['id']+1;//give the next id value
      }
      var_dump($id);
      $title =$_POST['eventname_input'];
      $date =$_POST['eventdate_input'];
      $description =$_POST['description_input'];
      //move upload file
      $uploads_dir = 'picture-event';
      $tmp_name = $_FILES['fileUploadHidden']['tmp_name'][$key];
      $name = $_FILES["fileUploadHidden"]["name"][$key];
      $picture_path = "$uploads_dir/$id$extension";
      move_uploaded_file($tmp_name, "../$uploads_dir/$id$extension");
      //send to bdd
      $stmt = $bdd->prepare("INSERT INTO event(title, description, id, picture_path,date) VALUES(?,?,?,?,?)");
      $stmt->bindParam(1, $title);
      $stmt->bindParam(2, $description);
      $stmt->bindParam(3, $id);
      $stmt->bindParam(4, $picture_path);
      $stmt->bindParam(5, $date);
      $stmt->execute();
    }
  }
}
header('Location: ../admin-addEvent.php');
  //
?>
