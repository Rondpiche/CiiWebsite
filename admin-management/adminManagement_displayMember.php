<?php
  $fileSettings = fopen("settings.txt", "r");
  $firstLine = intval(fgets($fileSettings));
  $databaseUsername = fgets($fileSettings);
  $databasePassword = fgets($fileSettings);
  $databaseName = fgets($fileSettings);
  $databaseUsername = substr($databaseUsername, 0, -2);
  $databasePassword = substr($databasePassword, 0, -2);
  fclose($fileSettings);
  try{
      $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
  }
  catch (Exception $e){
      die('Erreur : ' . $e->getMessage());
  }
  $reponse = $bdd->query("SELECT * FROM member");
  $i =1;
  while ($raw_data = $reponse->fetch()){
      echo '
      <tr id="eventRow'.$i.'" ng-click=clicked("'.$raw_data['id'].'") >
        <td class="" id="title'.$i.'" >'.$raw_data['name'].'</td>
        <td class="" id="description'.$i.'" >'.$raw_data['role'].'</td>
      </tr>';
      $i++;
  }
?>
