<?php
//check if at least one file was send
session_start();
if(isset($_SESSION['username']) && !empty($_SESSION['username'])){
  if (isset($_FILES["fileUploadHidden"]["error"]) !=false){
    //go though every files
    foreach ($_FILES["fileUploadHidden"]["error"] as $key => $error) {
      if ($error != UPLOAD_ERR_OK) {
        //not upload correctly
        break;
      }
      $extension =substr($_FILES['fileUploadHidden']['name'][$key] ,stripos($_FILES['fileUploadHidden']['name'][$key], '.') , strlen($_FILES['fileUploadHidden']['name'][$key]));
      if( !(($extension ==".jpg")||($extension ==".png")) ){
        //not the correct exention
        break;
      }
      if($_FILES['fileUploadHidden']['size'][$key] >10000000){
        //to Big
        break;
      }

      //!ADD MORE TEST LATER!//
      //pepare to send
      $fileSettings = fopen("../settings.txt", "r");
      $firstLine = intval(fgets($fileSettings));
      $databaseUsername = fgets($fileSettings);
      $databasePassword = fgets($fileSettings);
      $databaseName = fgets($fileSettings);
      $databaseUsername = substr($databaseUsername, 0, -2);
      $databasePassword = substr($databasePassword, 0, -2);
      fclose($fileSettings);
      try{
        $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
      }
      catch (Exception $e){
        die('Erreur : ' . $e->getMessage());
      }
      //retrive var
      $reponse = $bdd->query("SELECT * FROM member");
      $id =1;//default value
      while($raw_data = $reponse->fetch()){
        $id =$raw_data['id']+1;//give the next id value
      }
      $memberName =$_POST['membername_input'];
      $memberrole =$_POST['memberrole_input'];
      //move upload file
      $uploads_dir = 'picture-member';
      $tmp_name = $_FILES['fileUploadHidden']['tmp_name'][$key];
      $name = $_FILES["fileUploadHidden"]["name"][$key];
      $picture_path = "$uploads_dir/$id$extension";
      move_uploaded_file($tmp_name, "../$uploads_dir/$id$extension");
      //send to bdd
      $stmt = $bdd->prepare("INSERT INTO member(name, role,picture_path,id) VALUES(?,?,?,?)");
      $stmt->bindParam(1, $memberName);
      $stmt->bindParam(2, $memberrole);
      $stmt->bindParam(3, $picture_path);
      $stmt->bindParam(4, $id);
      $stmt->execute();
      var_dump($picture_path);
    }
  }
}
header('Location: ../admin-addMember.php');
?>
