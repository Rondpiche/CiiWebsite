<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CII</title><link rel="icon"
 type="image/png"
 href="asset\Cii logo.png">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Theme CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <!-- angular -->
    <script src="js/angular.js"></script>
    <!--script and css to create account and log in-->
    <script type="text/javascript" src="admin-management/adminManagement.js"></script>
    <script type="text/javascript" src="admin-management/sha512.js"></script>
    <link href="css/form.css" rel="stylesheet">
</head>
<body ng-app="main_ng-app">
  <?php include("asset/topbar.php") ?>
  <div class="container" style="margin-top:150px"  ng-show= <?php if(!$connected) echo 0; else echo 1;?>>
    	<div class="row">
			<div class="col-md-10 col-md-offset-1" >
				<div class="panel panel-login">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-6">
                <a href="#" class="active" id="login-form-link">Add member</a>
              </div>
              <div class="col-xs-6">
                <a href="#" id="register-form-link">Remove member</a>
              </div>
            </div>
            <hr>
          </div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">

                <!--login form-->
                <form id="login-form" name="myForm_logIn" ng-controller="form_controller" method="post" action ="admin-management/adminManagement_addMember.php" enctype="multipart/form-data">
                  <label>Information</label>
                  <div class="form-group">
                		<input id=membername name="membername_input" ng-model="name" type="text" ng-maxlength=255 tabindex="1" class="form-control" placeholder="member name" required>
                		  <div ng-show="!myForm_logIn.membername_input.$valid && submit ==1" class='callout alert' >A name must be given</div>
                      <div ng-show="myForm_logIn.membername_input.$error.maxlength && myForm_logIn.membername_input.$touched" class='callout alert'>name to long</div>
                  </div>
                  <div class="form-group">
                		<input id=memberrole name="memberrole_input" ng-model="role" type="text" ng-maxlength=255 tabindex="1" class="form-control" placeholder="member role" required>
                		  <div ng-show="!myForm_logIn.memberrole_input.$valid && submit ==1" class='callout alert' >A role must be given</div>
                      <div ng-show="myForm_logIn.memberrole_input.$error.maxlength && myForm_logIn.memberrole_input.$touched" class='callout alert'>role to long</div>
                  </div>

                  <label>picture</label>
                  <div class="form-group" >
                      <div class="input-group">
                          <label class="input-group-btn">
                              <span class="btn btn-primary" style="font-size: 11px; padding: 13px 20px; font-weight: 100;letter-spacing: 0px;">
                                  Browse<input type="file" style="display: none;" name ="fileUploadHidden[]" id="fileUploadHidden" required>
                              </span>
                          </label>
                          <input type="text" class="form-control" id="filesToUpload" readonly>
                      </div>
                      <span class="help-block">
                          Select one picture for your member
                      </span>
                      <div ng-show="submit ==1 && fileSend ==0" class='callout alert' >A picture must be given</div>
                  </div>
                  <input name="log_input" type="submit" class="form-control btn btn-login" value="Add" ng-click="submit()"/>
                </form>
                <!--delete member-->
                <form style="display: none;" id="register-form" name="myForm_removemember" ng-controller="form_controller">
                  <table class="table">
                    <thead class="thead-inverse">
                      <tr>
                        <th>name</th>
                        <th>role</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php include ("admin-management/adminManagement_displayMember.php"); ?>
                    </tbody>
                  </table>
                </form>
                <script>
                //Angular js controller
                var app = angular.module('main_ng-app', []);
                	app.controller('form_controller', function($scope) {
                    $scope.fileSend =0;
                    $scope.submit =0;
                    $scope.submit = function(){
                      $scope.submit =1;
                    }
                    $scope.fileSendNotifiedAngular = function(){
                        $scope.fileSend =1;
                    }
                    $scope.clicked =function($id){
                      var form = document.createElement("form");
                      form.method = 'post';
                      form.action = 'admin-management/adminManagement_removeMember.php';

                      var id = document.createElement('input');
                      id.type = "text";
                      id.name = "id";
                      id.value = $id;
                      form.appendChild(id);

                      form.setAttribute("style", "display : none");
                      document.body.appendChild(form);
                      form.submit();
                    }
                  });
                </script>
                <!--file upload-->
                <script>
                $(function() {
                  // We can attach the `fileselect` member to all file inputs on the page
                  $(document).on('change', ':file', function() {
                    var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                    input.trigger('fileselect', [numFiles, label]);
                  });
                  // We can watch for our custom `fileselect` member like this
                  $(document).ready( function() {
                      $(':file').on('fileselect', function(member, numFiles, label) {
                          var input = $(this).parents('.input-group').find(':text'),
                              log = numFiles > 1 ? numFiles + ' files selected' : label;
                          if( input.length ) {
                              input.val(log);//input the name of the var in the field
                              angular.element(document.getElementById('login-form')).scope().fileSendNotifiedAngular();
                          } else {
                              if( log ) alert(log);
                          }
                      });
                  });
                });
                </script>
                <!--switch between add and remove member-->
                <script>
                $(function() {

                    $('#login-form-link').click(function(e) {
                    $("#login-form").delay(100).fadeIn(100);
                    $("#register-form").fadeOut(100);
                    $('#register-form-link').removeClass('active');
                    $(this).addClass('active');
                    e.prmemberDefault();
                  });
                  $('#register-form-link').click(function(e) {
                    $("#register-form").delay(100).fadeIn(100);
                    $("#login-form").fadeOut(100);
                    $('#login-form-link').removeClass('active');

                    $(this).addClass('active');
                    e.prmemberDefault();
                  });

                });
                </script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
