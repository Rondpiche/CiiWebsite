<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CII</title><link rel="icon"
 type="image/png"
 href="asset\Cii logo.png">
    <link rel="icon"
     type="image/png"
     href="asset\Cii logo.png">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Theme CSS -->
    <link href="css/clean-blog.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- angular -->
    <script src="js/angular.js"></script>
</head>

<body>
    <?php include("asset/topbar-home.php") ?>
    <!-- Page Header -->
    <header class="intro-header" style="background-image: url('asset/home-page.jpg'); margin-bottom: 0px;" >
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1 style="font-size: 70px;">CII</h1>
                        <hr style="max-width: 300px;" class="small">
                        <span class="subheading">Club Informatique de l'Isen</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="row" style="background-color: #FFFFFF">
        <?php require("event-management/displayLastEvent.php") ?>
    </div>;

     <div class="row" style="background-color: #e5e5e5">
       <?php require("member-management/displayRandomMember.php") ?>
     </div>

     <div class="row" style="background-color: #FFFFFF">
       <div class="col-lg-2"></div>
       <div class="col-lg-4" style="text-align: left;margin-top: 80px;margin-bottom: 40px;">
         <h3>Où nous trouver ?:</h3>
         <p><a class="btn btn-default" href="locaux.php" role="button">nos locaux &raquo;</a></p>
       </div>
       <div class="col-lg-1"></div>
     <div class="col-lg-4" style="text-align: left;margin-top: 80px;margin-bottom: 40px;">
       <h3>Besoin d'aide ?:</h3>
       <p><a class="btn btn-default" href="" role="button">nous contacter &raquo;</a></p>
      </div>
      <div class="col-lg-1"></div>
    </div>
</body>
<style>
  p{
      font-size: 25px;
  }
  .h3, h3 {
      font-size: 45px;
  }
  .h1, h1{
      font-size: 35px;
  }
  .date-display{
    font-size: 18px;
  }
  .row {
    margin-right: 0px;
    margin-left: 0px;
  }
</style>
<?php include("asset/footer-home.php") ?>
</html>
