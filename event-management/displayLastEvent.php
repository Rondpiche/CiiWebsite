<?php
  $fileSettings = fopen("settings.txt", "r");
  $firstLine = intval(fgets($fileSettings));
  $databaseUsername = fgets($fileSettings);
  $databasePassword = fgets($fileSettings);
  $databaseName = fgets($fileSettings);
  $databaseUsername = substr($databaseUsername, 0, -2);
  $databasePassword = substr($databasePassword, 0, -2);
  fclose($fileSettings);
  try{
      $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
  }
  catch (Exception $e){
      die('Erreur : ' . $e->getMessage());
  }
  $reponse = $bdd->query("SELECT * FROM event");
  $i =0;
  $raw_data = $reponse->fetch();
  if($raw_data !=false){
    echo '<div class="col-lg-2"></div>
    <div class="col-lg-5" style="text-align: left;margin-top: 80px;margin-bottom: 80px;">
      <h3>Notre dernier événement:</h3>
      <h1>'.$raw_data["title"].'</h1>
      <p>'.$raw_data["description"].'</p>
      <p class ="date-display" style =" font-style: italic; color:rgb(128,128,128);"> <span class ="glyphicon glyphicon-calendar"></span>  '.$raw_data["date"].'</p>
       <p><a class="btn btn-default" href="event.php" role="button">Regarder nos autres événements &raquo;</a></p>
        </div>
        <div class="col-lg-4" style="text-align: center;margin-top: 80px;margin-bottom: 80px;">

      <img src="'.$raw_data["picture_path"].'" class="img-thumbnail" alt="événement" width="500px" height="400px">

      <div class="col-lg-2"></div>
    </div>';
  }
  else {
    echo '<div class="col-lg-2"></div>
    <div class="col-lg-5" style="text-align: left;margin-top: 80px;margin-bottom: 80px;">
      <h3>Notre dernier événement:</h3>
      <h1>Pas d\'événement enregistré</h1>
      <p> </p>
      <p class ="date-display" style =" font-style: italic; color:rgb(128,128,128);"> <span class ="glyphicon glyphicon-calendar"></span>  '.$raw_data["date"].'</p>
       <p><a class="btn btn-default" href="event.php" role="button">Regarder nos autres événements &raquo;</a></p>
        </div>
        <div class="col-lg-4" style="text-align: center;margin-top: 80px;margin-bottom: 80px;">

      <img class="img-thumbnail" alt="pas d\'événement enregistré" width="500px" height="400px">

      <div class="col-lg-2"></div>
    </div>';
  }
?>
