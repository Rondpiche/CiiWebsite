<?php
  $fileSettings = fopen("settings.txt", "r");
  $firstLine = intval(fgets($fileSettings));
  $databaseUsername = fgets($fileSettings);
  $databasePassword = fgets($fileSettings);
  $databaseName = fgets($fileSettings);
  $databaseUsername = substr($databaseUsername, 0, -2);
  $databasePassword = substr($databasePassword, 0, -2);
  fclose($fileSettings);
  try{
      $bdd = new PDO('mysql:host=localhost;dbname='.$databaseName.';charset=utf8', $databaseUsername, $databasePassword);
  }
  catch (Exception $e){
      die('Erreur : ' . $e->getMessage());
  }
  $reponse = $bdd->query("SELECT * FROM event");
  $i =0;
  while ($raw_data = $reponse->fetch()){
    if($i%2 ==0){
      echo'<div class="row" style="background-color: #e5e5e5">';
    }
    else{
      echo'<div class="row" style="background-color: #F5F5F5">';
    }
    echo'
    <div class="col-lg-5" style="text-align: right;margin-top: 80px;margin-bottom: 40px;">
      <img src="'.$raw_data["picture_path"].'" class="img-thumbnail" alt="Cinque Terre" width="450" height="345">
    </div>
    <div class="col-lg-5" style="text-align: left;margin-top: 80px;margin-bottom: 80px;">
      <h2>'.$raw_data["title"].'</h2>
      <p>'.$raw_data["description"].'</p>
      <p class ="date-display" style =" font-style: italic; color:rgb(128,128,128);"> <span class ="glyphicon glyphicon-calendar"></span>  '.$raw_data["date"].'</p>
    </div>
  </div>';
    $i++;
  }
  if($i ==0){
    echo'<div class="row" style="background-color: #e5e5e5">
      <div class="col-lg-5" style="text-align: right;margin-top: 80px;margin-bottom: 40px;">
        <h2>Pas encore d\'événement enregistré</h2>
      </div>
    </div>';
    include("asset/footer.php");
  }else{
    include("asset/footer-home.php");
  }
?>
