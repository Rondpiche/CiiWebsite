<!-- Footer -->
<footer class="footer" style="position: absolute; bottom: 0;background-color:#F0F0F0;width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p class="copyright text-muted">Copyright &copy; Marmoset Survey 2017</p>
            </div>
        </div>
    </div>
</footer>
