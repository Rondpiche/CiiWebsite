<!--Part that change the color of the nav bar when scrolling-->
<script>
  $(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar-fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
});
</script>

<style>
  .navbar-fixed-top.scrolled li a.logo{
    filter: invert(100%);
  }
  .navbar-fixed-top.scrolled {
    background-color: #F5F5F5 !important;
    transition: background-color 200ms linear;
  }
  .navbar-fixed-top.scrolled li a{
      color: #050505;
  }
  .navbar-fixed-top.scrolled li a:hover{
      color: #0085A1;
  }
</style>
<!--session start-->
<?php
  session_start();
  if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
     $connected =TRUE;
     echo '<script type="text/javascript" src="admin-management/adminManagement.js"></script>';
  }else{
      $connected =FALSE;
  }
?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top" ng-app="nav_ng-app">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span style="font-size:1.4em;" class="glyphicon glyphicon-align-justify"></span>
                <span style="font-size:0.7em;" class="glyphicon glyphicon-triangle-bottom"></span>
              </a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">cii</li>
                  <li><a href="event.php">événement</a></li>
                  <li><a href="membre.php">membre</a></li>
                  <li><a href="locaux.php">locaux</a></li>
                <li role="separator" class="divider"></li>
              </ul>
            </li>
            <li style="margin-right: -20px; margin-left: -20px;  margin-top: -10px;">
              <a style="pointer-events: none" class ="logo">
                <img style="filter: invert(100%);"
                  src="asset/Cii logo.svg"
                  alt="cii logo"
                  height="30pem"
                  width="30pem" />
              </a>
            </li>
            <li>
                <a href="index.php">Club informatique isen</a>
            </li>
          </ul>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="<?php if(!$connected) echo ""; else echo ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"'?>">
                  <!--display the username if connected and log in otherwise-->
                  <?php
                    if($connected){
                      echo '<span class="glyphicon glyphicon-user"></span>  ';
                      echo $_SESSION['username'];
                      echo ' <span style="font-size:0.7em;" class="glyphicon glyphicon-triangle-bottom"></span>';
                      echo '<script type="text/javascript" src="admin-management/adminManagement-logout.js"></script>';//needed only if connected
                    }
                    else echo "";
                  ?>
                </a>
                <ul class="dropdown-menu"  ng-show= <?php if($connected) echo 1; else echo 0;?>>
                  <li class="dropdown-header">Admin</li>
                    <li><a href="" onclick="logOut()">Log out</a></li>
                </ul>
              </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!--angular script-->
<script>
  var app = angular.module('nav_ng-app', []);
  app.controller('form_controller', function($scope) {

  });
</script>
